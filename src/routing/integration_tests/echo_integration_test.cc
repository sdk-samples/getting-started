// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// [START example_snippet]
#include <fidl/examples.routing.echo/cpp/fidl.h>
#include <gtest/gtest.h>
#include <lib/component/incoming/cpp/protocol.h>

#include <string>

TEST(EchoIntegrationTest, TestEcho) {
  zx::result client_end = component::Connect<examples_routing_echo::Echo>();
  ASSERT_TRUE(client_end.is_ok());
  fidl::SyncClient client{std::move(*client_end)};

  std::string request("Hello, world!");
  fidl::Result result = client->EchoString({request});
  ASSERT_TRUE(result.is_ok());

  auto response = result->response();
  ASSERT_TRUE(response.has_value());
  ASSERT_EQ(request, response.value());
}
// [END example_snippet]
