// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// [START imports]
#include <fidl/examples.routing.echo/cpp/fidl.h>
#include <lib/component/incoming/cpp/protocol.h>
#include <lib/syslog/global.h>

#include <cstdlib>
#include <iostream>
#include <string>
// [END imports]

// [START main_body]
int main(int argc, const char *argv[], char *envp[]) {
  // [START_EXCLUDE silent]
  // TODO(fxbug.dev/97170): Consider migrating to async FIDL API
  // [END_EXCLUDE]
  // Connect to FIDL protocol
  zx::result client_end = component::Connect<examples_routing_echo::Echo>();
  if (!client_end.is_ok()) {
    FX_LOGF(ERROR, "echo_client", "Error connecting to Echo protocol: %s",
            client_end.status_string());
    return -1;
  }
  fidl::SyncClient client{std::move(*client_end)};

  // Send messages over FIDL interface for each argument
  for (int i = 1; i < argc; i++) {
    fidl::Result result = client->EchoString({argv[i]});
    ZX_ASSERT(result.is_ok());

    auto response = result->response();
    if (!response.has_value()) {
      FX_LOG(INFO, "echo_client", "echo_string got empty result");
    } else {
      FX_LOGF(INFO, "echo_client", "Server response: %s", response->c_str());
    }
  }

  return 0;
}
// [END main_body]
