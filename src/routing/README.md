# Routing Example

This directory contains an example of
[capability routing](https://fuchsia.dev/fuchsia-src/concepts/components/v2/component_manifests)
in [Component Framework](https://fuchsia.dev/fuchsia-src/concepts/components/v2/introduction).

## Building

These components are built with
```bash
$ bazel build --config=fuchsia_x64 src:fuchsia_samples
```
as part of the other samples in this repository. Reference the top level README for more details.

## Running

Use `ffx component create` to create the component instances inside a restricted
realm for development purposes:

```bash
$ tools/ffx component create /core/ffx-laboratory:echo_realm fuchsia-pkg://fuchsiasamples.com/components_routing_example#meta/echo_realm.cm
```

Start the client component instance by passing its moniker to
`ffx component start`:

```bash
$ tools/ffx component start /core/ffx-laboratory:echo_realm/echo_client
```

When the above command is run, you can see the following output with `tools/ffx log --filter echo_client`:

```
[echo_client] INFO: Server response: Hello Fuchsia!
```

## Testing

Integration tests for echo server are available in the `echo_integration_test`
package. Use the `ffx test run` command to run the tests on a target device:

```bash
$ tools/ffx test run fuchsia-pkg://fuchsiasamples.com/echo_integration_test#meta/echo_integration_test.cm
```

You should see the integration tests execute and pass:

```
Running test 'fuchsia-pkg://fuchsiasamples.com/echo_integration_test#meta/echo_integration_test.cm'
[RUNNING]       EchoIntegrationTest.TestEcho
[stdout - EchoIntegrationTest.TestEcho]
Running main() from gmock_main.cc
[PASSED]        EchoIntegrationTest.TestEcho

1 out of 1 tests passed...
fuchsia-pkg://fuchsiasamples.com/echo_integration_test#meta/echo_integration_test.cm completed with result: PASSED
```
