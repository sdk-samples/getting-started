# Echo Example

This directory contains a simple example using program arguments and environment
variables in [Component Framework](https://fuchsia.dev/fuchsia-src/concepts/components/v2/introduction).

## Running

Use `ffx component run` to launch this component into a restricted realm
for development purposes:

```bash
tools/ffx component run fuchsia-pkg://fuchsiasamples.com/echo-example#meta/echo.cm
```

When the above command is run, you can see the following output with `ffx log`:

```
[echo] INFO: Hello, Alice, Bob, Spot!
```

## Testing

Unit tests for echo are available in the `echo-unittests` package.
Use the `ffx test run` command to run the tests on a target device:

```bash
$ tools/ffx test run fuchsia-pkg://fuchsiasamples.com/echo_unittests#meta/echo_unittests.cm
```

You should see each of the unit tests execute and pass:

```
Running test 'fuchsia-pkg://fuchsiasamples.com/echo_unittests#meta/echo_unittests.cm'
[RUNNING]       EchoTest.TestGreetOne
[stdout - EchoTest.TestGreetOne]
Running main() from gmock_main.cc
[PASSED]        EchoTest.TestGreetOne
[RUNNING]       EchoTest.TestGreetTwo
[stdout - EchoTest.TestGreetTwo]
Running main() from gmock_main.cc
[PASSED]        EchoTest.TestGreetTwo
[RUNNING]       EchoTest.TestGreetThree
[stdout - EchoTest.TestGreetThree]
Running main() from gmock_main.cc
[PASSED]        EchoTest.TestGreetThree

3 out of 3 tests passed...
fuchsia-pkg://fuchsiasamples.com/echo_unittests#meta/echo_unittests.cm completed with result: PASSED
```
