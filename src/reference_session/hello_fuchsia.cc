// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <iostream>

#include "hello_fuchsia.h"

void YourMessage(const std::string &message) {
  std::cout << message << std::endl;
}
