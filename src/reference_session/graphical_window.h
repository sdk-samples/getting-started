// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_GRAPHICAL_WINDOW_H_
#define FUCHSIA_SDK_EXAMPLES_GRAPHICAL_WINDOW_H_

#include <fidl/fuchsia.element/cpp/fidl.h>
#include <fidl/fuchsia.ui.composition/cpp/fidl.h>
#include <fidl/fuchsia.ui.views/cpp/fidl.h>
#include <lib/async-loop/cpp/loop.h>
#include <lib/async-loop/default.h>
#include <lib/fidl/cpp/binding_set.h>

class GraphicalWindow {
public:
  explicit GraphicalWindow(async_dispatcher_t *dispatcher);
  void CreateView();

private:
  void GetLayout();
  void DrawSquare(int32_t window_width, int32_t window_height,
                  uint32_t square_size);
  void OnGetLayout(
      fidl::Result<fuchsia_ui_composition::ParentViewportWatcher::GetLayout>
          &result);
  async_dispatcher_t *dispatcher_;

  fidl::Client<fuchsia_ui_composition::Flatland> flatland_;
  fidl::Client<fuchsia_ui_composition::ParentViewportWatcher>
      parent_viewport_watcher_;
  fidl::Client<fuchsia_element::GraphicalPresenter> presenter_;
};

#endif // FUCHSIA_SDK_EXAMPLES_GRAPHICAL_WINDOW_H_
