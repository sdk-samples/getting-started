// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_HELLO_FUCHSIA_H_
#define FUCHSIA_SDK_EXAMPLES_HELLO_FUCHSIA_H_

#include <string>

void YourMessage(const std::string &message);

#endif // FUCHSIA_SDK_EXAMPLES_HELLO_FUCHSIA_H_
