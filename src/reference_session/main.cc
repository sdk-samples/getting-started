// Copyright 2024 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <iostream>
#include <lib/async-loop/cpp/loop.h>
#include <lib/async-loop/default.h>
#include <lib/sys/cpp/component_context.h>
#include <string>

#include "graphical_window.h"
#include "hello_fuchsia.h"

int main() {
  std::cout << "Welcome to Fuchsia!" << std::endl;

  // Here, you can change the message that you want to display on the tools/ffx
  // logs!
  std::string message = "Develop on Fuchsia!";

  YourMessage(message);

  async::Loop loop(&kAsyncLoopConfigAttachToCurrentThread);
  auto graphical_window = std::make_unique<GraphicalWindow>(loop.dispatcher());
  graphical_window->CreateView();
  loop.Run();
  return 0;
}
