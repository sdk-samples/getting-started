# Fuchsia samples using the Fuchsia SDK

This repository contains instructions and source code to build, package and run
Fuchsia samples using only the Fuchsia SDK.

## Set up and bootstrap the SDK samples repository

Clone this SDK samples repository on your host machine. This repository contains
the Bazel-based Fuchsia SDK and sample components.

The tasks include:

- Bootstrap the SDK samples repository.
- Download the SDK toolchain to initialize the SDK environment.
- Verify that you can run [`ffx`][ffx] commands.

Do the following:

1. Open a terminal.
1. In the terminal, change to your home directory:

   ```posix-terminal
   cd
   ```

1. Clone the Fuchsia samples repository:

   ```posix-terminal
   git clone https://fuchsia.googlesource.com/sdk-samples/getting-started fuchsia-getting-started --recurse-submodules
   ```

   This `git clone` command creates a new directory named
   `fuchsia-getting-started` and clones the content of the
   [SDK samples repository][sdk-samples-repo]{:.external}.

1. Go to the new directory:

   ```posix-terminal
   cd fuchsia-getting-started
   ```

1. Run the bootstrap script to install Bazel and other required dependencies:

   ```posix-terminal
   scripts/bootstrap.sh
   ```

1. To verify that you can use the `ffx` tool in your environment, run the
   following command:

   ```posix-terminal
   tools/ffx sdk version
   ```

   This command prints output similar to the following:

   ```none {:.devsite-disable-click-to-copy}
   $ tools/ffx sdk version
   21.20240623.2.1
   ```

   At this point, you only need to confirm that you can run `ffx` commands
   without error.

   Note: The output above shows the version `21.20240623.2.1`, which
   indicates that this SDK was built and published on June 23, 2024.

## Building and flashing the reference product to vim3

Once you have downloaded the toolchain, you will be able to build products.
Here, you will need a vim3 plugged into your computer to build and flash a
product bundle to it. Remote desktops may not allow for flashing to vim3.

1. Make sure the device is in fastboot:

   ```posix-terminal
   tools/ffx target reboot -b
   ```

1. Check for the target:

   ```posix-terminal
   tools/ffx target list
   ```

   This should print an output similar to the following

   ```posix-terminal
   NAME                      SERIAL          TYPE       STATE      ADDRS/IP                            RCS
   fuchsia-c863-1474-66db    c863147466db    Unknown    Fastboot    [fe80::37a4:2a98:628b:3687%eth0,    Y
                                                                 172.16.243.55]
   ```

1. Flash and build the reference product:

   ```posix-terminal
   tools/bazel run products/reference:reference.vim3.flash
   ```

   This should print an output similar to the following:

   ```posix-terminal
   WARNING: Build option --run_under has changed, discarding analysis cache (this can be expensive, see https://bazel.build/advanced/performance/iteration-speed).
   INFO: Analyzed target //products/reference:reference.vim3.flash (0 packages loaded, 7093 targets configured).
   INFO: Found 1 target...
   Target //products/reference:reference.vim3.flash up-to-date:
    bazel-bin/products/reference/reference.vim3.flash.sh
    bazel-bin/products/reference/reference.vim3.flash_workflow.json
   INFO: Elapsed time: 0.273s, Critical Path: 0.03s
   INFO: 1 process: 1 internal.
   INFO: Build completed successfully, 1 total action
   INFO: Running command line: bazel-bin/products/reference/_reference.vim3.flash.sh
   Running task: reference.vim3.flash (step 1/1)
   No `--authorized-keys` flag, using /home/$user/.ssh/fuchsia_authorized_keys
   Preparing to upload /home/$user/.cache/bazel/_bazel_$user/98f4d94884500639d789df9d78c04f15/execroot/__main__/bazel-out/k8-fastbuild/bin/products/reference/reference.vim3_out/partitions/u-boot.bin.unsigned
   Uploading... Done [0.08s]
   Partitioning bootloader... Done [0.18s]
   Rebooting to bootloader... Done [4.53s]
   Preparing to upload /home/$user/.cache/bazel/_bazel_$user/98f4d94884500639d789df9d78c04f15/execroot/__main__/bazel-out/k8-fastbuild/bin/products/reference/reference.vim3_out/system_a/fuchsia.zbi
   Uploading... Done [0.67s]
   Partitioning zircon_a... Done [0.61s]
   Preparing to upload /home/$user/.cache/bazel/_bazel_$user/98f4d94884500639d789df9d78c04f15/execroot/__main__/bazel-out/k8-fastbuild/bin/products/reference/reference.vim3_out/system_a/fuchsia.vbmeta
   Uploading... Done [0.00s]
   Partitioning vbmeta_a... Done [0.03s]
   Preparing to upload /home/$user/.cache/bazel/_bazel_$user/98f4d94884500639d789df9d78c04f15/execroot/__main__/bazel-out/k8-fastbuild/bin/products/reference/reference.vim3_out/system_a/fxfs.sparse.blk
   Uploading... large file, please wait... Done [3.13s]
   Partitioning fvm... Done [2.42s]
   Preparing to stage /home/$user/.ssh/fuchsia_authorized_keys
   Uploading... Done [0.00s]
   Sending command "add-staged-bootloader-file ssh.authorized_keys"
   Continuing to boot - this could take awhile
   Done. Total Time [11.82s]
   ```

Now you have successfully flashed the reference product to a vim3. You can use
the command below to ssh into the target:

```posix-terminal
tools/ffx target ssh
```

Use the command below to find more commands you can use with your flashed target:

```posix-terminal
tools/ffx target help
```

## Booting up a Fuchsia emulator

Something else you can do is boot the product in an emulator

1. Build and start the emulator

   ```posix-terminal
   tools/bazel run products/reference:reference.x64.emu
   ```

   Optionally, if you want to ssh into the emulator instead of
   using the terminal window, use the following command.

   ```posix-terminal
   tools/ffx target ssh
   ```

## Running a session on the emulator

A session is a component. Each Fuchsia product builds its user experience
within a session

1. Once your emulator is running, you can see that a terminal window pops up on
the screen that shows a fuchsia colored square in the middle. This is part of
the session that automatically runs when you run the product. Run
the following command to see the logs of your session.

   ```posix-terminal
   tools/ffx log --filter reference_session
   ```

   This should print an output similar to the following:

   ```posix-terminal
   [01546.453857][session:session] INFO: Welcome to Fuchsia!
   [01546.453896][session:session] INFO: Develop on Fuchsia!
   ```

   Optionally, to stop the session, run the following command

   ```posix-terminal
   tools/ffx session stop
   ```

   You will see that the terminal window no longer has the colored square.

Now, feel free to add your own message to this binary that the session's component runs
and change the color of the square in these following files, respectively.

   ```posix-terminal
   src/reference_session:main.cc
   ```

   ```posix-terminal
   src/reference_session:graphical_window.cc
   ```

Close the terminal window and rerun the emulator to test your changes.
<!-- Reference links -->

[clang]: https://clang.llvm.org
[ffx]: https://fuchsia.dev/reference/tools/sdk/ffx
[fuchsia-idk]: https://fuchsia.dev/fuchsia-src/development/idk
[sdk-samples-repo]: https://fuchsia.googlesource.com/sdk-samples/getting-started
