#!/bin/bash

# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


# Run this from the root of the repo with `scripts/smoke_test.sh`
set -e -o pipefail

emu_name=_smoke_test

if [[ -z "$HOME" ]]; then
  export HOME=$(pwd)
fi

failure() {
    echo >&2 "ERROR: $0 line $1"
}

print_and_run() {
  echo
  echo "*** $@"
  "$@"
}

wait_for_emu_rcs() {
  local i=0
  while [[ $i -lt 120 ]] &&
      [[ "$(tools/ffx target list 2>&1 | grep $emu_name | awk '{print $6;}')" != "Y" ]]; do
    echo -n .
    sleep 0.5
    i+=1
  done
  echo
}

main() {
  trap 'failure $LINENO' ERR
  local keep_emu=0
  local skip_clean=0
  while [[ $# -gt 0 ]]; do
    if [[ "$1" == "--keep" ]]; then
      keep_emu=1
    elif [[ "$1" == "--no-clean" ]]; then
      skip_clean=1
    else
      echo >&2 "Invalid argument: $1"
      echo >&2 "Syntax: $0 [--keep] [--no-clean]"
      return 1
    fi
    shift
  done

  print_and_run scripts/bootstrap.sh

  echo "Build starting!"

  if [[ $skip_clean -eq 0 ]]; then
    # clear previously fetched dependencies to ensure a clean environment
    print_and_run tools/bazel clean --expunge
  fi

  # build all samples in this repository
  print_and_run tools/bazel build src:fuchsia_samples

  print_and_run tools/ffx doctor --restart-daemon
  print_and_run tools/ffx sdk version

  current_target="$(tools/ffx target default get)"

  pb_name="workstation_eng.qemu-x64"
  repo_name="workstation-packages"

  # remove the existing repository (ignore error in case it doesn't exist)
  print_and_run tools/ffx repository remove "$repo_name" || true

  # fetch an emulator image of workstation and start an emulator
  print_and_run tools/ffx product-bundle get "$pb_name" --repository "$repo_name" --force-repo
  stop_emu

  print_and_run tools/ffx emu start "$pb_name" --headless --name $emu_name

  if [[ $keep_emu -eq 0 ]]; then
    trap "stop_emu" EXIT
  fi

  wait_for_emu_rcs

  # start the package server
  print_and_run tools/ffx repository server start

  print_and_run tools/ffx target default set $emu_name

  # register the package repository with on-demand prebuilt packages
  print_and_run tools/ffx target repository register -r $repo_name --alias fuchsia.com --alias chromium.org
  print_and_run tools/ffx repository list

  # run tests on the emulator
  print_and_run tools/bazel test "src/hello_world:test_pkg"
  print_and_run tools/bazel test "src/echo:test_pkg"

  # TODO(fxbug.dev/103976): uncomment after bug is fixed
  # print_and_run tools/bazel test "src/routing:test_pkg"

  # other commands used in https://fuchsia.dev/fuchsia-src/get-started/sdk
  # CQ actually uses mawk and not awk or gawk, so does not support symbols in search string
  check_output() {
    if ! awk -v SEARCH="$@" '$0 ~ SEARCH{rc=1; print "Found match: "$0}{print $0}; END{exit !rc}'; then
      echo "Failed to find expected string: $@" >&2
      exit 1
    fi
  }
  print_and_run tools/bazel run --config=fuchsia_x64 //src/hello_world:pkg.component
  print_and_run tools/ffx component show hello_world | check_output "Moniker:  /core/ffx-laboratory:hello_world"
  print_and_run tools/ffx daemon stop
  print_and_run tools/ffx inspect list | check_output "bootstrap/archivist"
  print_and_run tools/ffx inspect selectors bootstrap/archivist | check_output "bootstrap/archivist:root/archive_accessor_stats/all/inspect"
  print_and_run tools/ffx inspect show bootstrap/archivist:root/events/recent_events | check_output "filename = fuchsia.inspect.Tree"
  print_and_run tools/bazel test --config=fuchsia_x64 --test_output=all //src/hello_world:test_pkg.hello_gtest | check_output "PASSED"
  test_zxdb() { echo quit | tools/ffx debug connect; }
  print_and_run test_zxdb | check_output "Connected successfully"
  # other commands used in https://fuchsia.dev/fuchsia-src/get-started/sdk/learn/components
  print_and_run tools/bazel run --config=fuchsia_x64 //src/echo:pkg.publish -- --repo_name fuchsiasamples.com | check_output "Published 1 packages"
  print_and_run tools/ffx component create /core/ffx-laboratory:echo fuchsia-pkg://fuchsiasamples.com/echo-example#meta/echo.cm | check_output "Creating component instance"
  print_and_run tools/ffx component start /core/ffx-laboratory:echo | check_output "Started component instance"
  print_and_run tools/ffx component destroy /core/ffx-laboratory:echo | check_output "Destroying component instance"
  print_and_run tools/ffx component run /core/ffx-laboratory:echo fuchsia-pkg://fuchsiasamples.com/echo-example#meta/echo.cm | check_output "Started component instance"
  print_and_run tools/bazel run --config=fuchsia_x64 //src/echo:pkg.publish -- --repo_name fuchsiasamples.com | check_output "Published 1 packages"
  print_and_run tools/ffx component run /core/ffx-laboratory:echo fuchsia-pkg://fuchsiasamples.com/echo-example#meta/echo.cm --recreate | check_output "Started component instance"
  print_and_run tools/ffx component destroy /core/ffx-laboratory:echo | check_output "Destroying component instance"
  # other commands used in https://fuchsia.dev/fuchsia-src/get-started/sdk/learn/fidl/fidl
  print_and_run tools/bazel run --config=fuchsia_x64 //src/routing:pkg.publish -- --repo_name fuchsiasamples.com | check_output "Published 1 packages"
  print_and_run tools/ffx component create /core/ffx-laboratory:echo-realm fuchsia-pkg://fuchsiasamples.com/components_routing_example#meta/echo_realm.cm | check_output "Moniker: /core/ffx-laboratory:echo-realm"
  print_and_run tools/ffx component resolve /core/ffx-laboratory:echo-realm
  print_and_run test_fidlcat
  # other commands used in https://fuchsia.dev/fuchsia-src/get-started/sdk/learn/fidl/integration-testing
  print_and_run tools/bazel build //src/routing/integration_tests:test_pkg
  print_and_run tools/bazel run //src/routing/integration_tests:test_pkg.publish -- --repo_name fuchsiasamples.com
  # CQ uses a non-HOME directory for .ssh so pass in the private key directly for all cases
  print_and_run tools/fssh -private-key "${HOME}/.ssh/fuchsia_ed25519" uname -a | check_output "Zircon"

  echo "Success!"
}
test_fidlcat() {
  local bazel_out
  bazel_out="$(tools/bazel info output_path)"
  timeout 10s tools/ffx debug fidl --remote-name echo_server.cm --fidl-ir-path "$bazel_out" || if [ "$?" != "124" ]; then /bin/false; fi
}

stop_emu() {
  # stop the emulator
  if tools/ffx emu show $emu_name >/dev/null 2>&1; then
    tools/ffx emu stop $emu_name || true
  fi
  if [[ -n "${current_target}" ]]; then
    tools/ffx target default set "${current_target}" || true
  else
    tools/ffx target default unset || true
  fi

  return 0
}

time main "$@"
