# Create a product with the Fuchsia SDK

This codelab explains how you can create your own product bundle and
build your target with a custom configuration.

A Fuchsia product bundle includes system images, packages, and metadata that can be
used to flash, emulate, or update a Fuchsia target. Product Bundles are built by
providing:

* Product configurations which describe the user experience
* Board configurations which describe the target hardware
* Partitions configurations which describe how to map images to physical partitions

Besides providing product-specific code, product configurations can configure the
Fuchsia platform and indicate requirements from the product. Board configurations
identify available capabilities of the target hardware. When a product bundle is
being constructed, the Fuchsia tooling will assert that all the requirements of
the product are met by the board.

## Write first product rules

In the `//products/reference/BUILD.bazel` file, you can see the `fuchsia_product`
and `fuchsia_product_bundle` Bazel rules.

A `fuchsia_product` combines a product and board configuration to create a set of
images for a single physical slot (A, B, or R). On bootup, the bootloader typically
chooses which slot to boot into. When we flash a target, we typically provide A and
R slots, which is the "main" product and the "recovery" product.

A `fuchsia_product_bundle` takes multiple `fuchsia_product`s and puts them into a single
distributable directory. It also contains the packages and metadata that are used to
emulate, flash, and ota these products to a fuchsia target.

The `//products/reference/BUILD.bazel` file contains examples of
attributes you can use in your product.

For more information on the `fuchsia_product` and `fuchsia_product_bundle`
Bazel rules, see the [`fuchsia_product`][fuchsia-product] and
[`fuchsia_product_bundle`][fuchsia-product-bundle], respectively.

## Choose board configuration

In the `//boards/BUILD.bazel` file, there is a Bazel rule for prebuilt boards.
Using this file as an example, you can choose one of the boards for your product.
Alternatively, you can also download a different prebuilt board definition from
the [CIPD repository][CIPD]. Start by going to this CIPD link for the [boards][CIPD-boards].
After that, look into the `manifests` directory and `WORKSPACE.bazel` file to look at how
to import these boards.

## Choose partition configuration

In the `//boards/partitions/BUILD.bazel` file, there is a Bazel rule for prebuilt
partition configurations. Partitions are the areas in storage that can contain the
system images. The partitions configuration indicates which partitions a specific
system image should be flashed onto. Using this file as an example, you can choose
one of the partitions configurations for your product.

## Specify product configuration

In the `//products/reference/BUILD.bazel` there is a Bazel rule called
`fuchsia_product_configuration` that was written for the existing reference
product. Using this as an example, you can make your own product definition
in the `//products/codelab/BUILD.bazel` file. See the
[ProductConfig][product-config-ref] for a list of supported fields for the
`product_config` attribute.

## Build

After specifying board, partition, and product configurations, you are now ready to build
your product with the following command.

```
tools/bazel build products/codelab:your_product_name
```

Depending on your product, you may also want to add additional attributes like
virtual devices and binaries that you may want to run. To learn more about developing
on Fuchsia, see:

- [Open Source Fuchsia Repository][fuchsia-open-source]
- [Fuchsia Bazel Rules][bazel-rules]
- [Fuchsia.dev][fuchsiadev]
- [Basic Concepts of Bazel][bazel-concepts]
- [Prebuilt Boards][boards]

**Congratulations on writing your first product!**

You have created your own product on Fuchsia using only the Fuchsia SDK!

[fuchsia-open-source]: https://cs.opensource.google/fuchsia/fuchsia/+/main:
[bazel-rules]: https://cs.opensource.google/fuchsia/fuchsia/+/main:build/bazel_sdk/bazel_rules_fuchsia/fuchsia/private/assembly/
[fuchsiadev]: https://fuchsia.dev/
[bazel-concepts]: https://bazel.build/concepts/build-ref
[boards]: https://cs.opensource.google/fuchsia/fuchsia/+/main:boards/
[CIPD-boards]: https://chrome-infra-packages.appspot.com/p/fuchsia/assembly/boards
[fuchsia-product-bundle]: https://fuchsia.dev/reference/bazel_sdk/fuchsia_product_bundle
[fuchsia-product]: https://fuchsia.dev/reference/bazel_sdk/fuchsia_product
[CIPD]: https://chrome-infra-packages.appspot.com/p/fuchsia
[product-config-ref]: https://fuchsia.dev/reference/assembly/ProductConfig